// Constructor
function person(name) {
  this.name = name;

  this.speak = function () {
    console.log(this.name + ' is aan het praten.');
  }
}

// Prototype
person.prototype.walk = function () {
  console.log(this.name + ' is aan het lopen.');
}

person.prototype.eat = function () {
  console.log(this.name + ' is aan het eten.');
}

var bob = new person('Bob');

// Object Literal
var bobLiteral = {
  name: 'Bob',
  speak: function () {
    console.log(name + ' is aan het praten.');
  },
  walk: function () {
    console.log(name + ' is aan het lopen.');
  },
  eat: function () {
    console.log(name + ' is aan het eten.');
  }
}