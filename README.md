# FED 2 Laurens Bruijn
500666825, klas 2

## Best Practices

### Algemeen

### JavaScript
* functies, variables en objecten in camelCase
* constructor objecten beginnen met hoofdletter
* alleen een spatie voor de accolade van een functie, dus zo: function() {...
* line-break tussen de properties en methods van een object
* 2 spaties voor inspringing

### CSS
* gebruik van SASS
* line-break tussen selectors op hetzelfde niveau
* eerst properties, dan andere selectors binnen dezelfde selector (gescheiden met een line-break)
* naamconventie: .module .module-submodule .module--subcomponent
* 2 spaties voor inspringing

### HTML
* data attributes gebruiken voor javascript
* externe scripts aan de onderkant van de pagina
* 2 spaties voor inspringing